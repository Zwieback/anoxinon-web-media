+++
title = "Downloads"
description = "Downloads"
keywords = ["extras","Downloads","Links","Anxicon"]


+++

Nachfolgend eine Auflistung von Downloads:

---

**Flyer**

- [Anleitung für Conversations](/files/Anleitung_Conversations.pdf) - Von Messtome, [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)
- [Gründe für Linux - Website Edition](/files/Flyer_Linux_final_Webseite.pdf) - Von Anoxinon & Messtome, [CC-BY 4.0](/lizenzhinweis/)
- [Datenschutz für Klein & Gross - Website Edition](/files/Flyer_Datenschutz_final_Webseite.pdf) - Von Anoxinon & Messtome, [CC-BY 4.0](/lizenzhinweis/)

---

> Die Auflistung ist derzeit unvollständig
