+++
title = "Impressum"
+++

### Verantwortlich im Sinne des TMG:

**Anoxinon e.V.**

c/o Patrick Heinrich von den Driesch

Sieweckestraße 11

33330 Gütersloh

Deutschland

Amtsgericht Potsdam  
Registernummer: 9009

Vertretungsberechtigter Vorstand: Lukas Nerantzis, York-Simon Johannsen, Michael Gulden, Malte Kiefer, Patrick Heinrich von den Driesch

#### Inhaltlich Verantwortlicher gemäß Rundfunkstaatsvertrag (RStV):

Patrick Heinrich von den Driesch  
c/o Anoxinon e.V.  
Sieweckestraße 11

33330 Gütersloh


### Kontakt:

Telefon: (+49) 0152 06482057

E-Mail: postfach[ett]anoxinon(dot)de

### Lizenzhinweis Design

Original Template von [Bootstrapious](http://bootstrapious.com/free-templates), angepasst für Hugo durch [DevCows](https://github.com/devcows/hugo-universal-theme), Eigene Änderungen wurden vorgenommen.

### Adressverarbeitung

Alle die auf dieser Webseite angegebenen Kontaktinformationen von Personen,  
inklusive etwaiger Fotos dienen ausdrücklich nur zu Informationszwecken bzw.  
zur Kontaktaufnahme. Sie dürfen insbesondere nicht für die Zusendung von Werbung,  
Spam und ähnliches genutzt werden. Einer werblichen Nutzung dieser Daten wird  
deshalb hiermit widersprochen. Sollten diese Informationen dennoch zu den vorstehend  
genannten Zwecken genutzt werden, behalten wir uns etwaige rechtliche Schritte vor.

### Abmahnungsrechtlicher Hinweis

Eine Verletzung von Schutzrechten Dritter oder eine Situation, die eine Aufforderung  
per anwaltlicher Abmahnung motivieren könnte, entspricht nicht dem mutmaßlichen  
Willen der Betreiber dieser Webseiten. In jedem Fall sichern wir die sofortige Behebung  
der Verletzung zu. Kosten einer Abmahnung oder einer anwaltlichen Beratung können wir  
daher als offenkundlich mißbräuchlich nicht übernehmen.

---

## Haftungsausschluss (Disclaimer)

### Haftung für Inhalte

Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten  
nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter  
jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu  
überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.  
Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den  
allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst  
ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden  
von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.

### Haftung für Links

Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen  
Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen.  
Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der  
Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche  
Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.  
Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete  
Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen  
werden wir derartige Links umgehend entfernen.

### Urheberrecht

Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen  
dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der  
Verwertung außerhalb der Grenzen des Urheberrechtes bzw. [der erteilten Lizenz](/lizenzhinweis) bedürfen der schriftlichen Zustimmung des  
jeweiligen Autors bzw. Ersteller. Soweit die Inhalte auf dieser Seite nicht vom Betreiber  
erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter  
als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden,
bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir
derartige Inhalte umgehend entfernen.
