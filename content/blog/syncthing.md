---
title: Dateisynchronisation mit Syncthing
date: 2021-04-03T00:00:00+00:00
tags:
- Fortgeschritten
categories:
- Freie Software
banner: "/img/thumbnail/syncthing.png"
description: Syncthing ist eine freie Anwendung zur Dateisynchronisation ohne zentrale Cloud. In diesem Beitrag gehen wir auf die Benutzung und die dahinterstehende Technologie ein.
---

Es ist ein Problem, das viele sicherlich kennen: Man hat mehrere Geräte und möchte bestimmte Dateien auf allen Geräten immer griffbereit haben.
Ein USB-Datenträger, den man immer an das momentan benutzte Gerät anschließt, ist natürlich keine schöne Lösung.
Eine andere naheliegende Lösung sind Cloudspeicher - Onlinedienste, bei denen man einen Ordner hat, in den man Dateien speichern kann. Dazu gibt
es oft Clientanwendungen, die einen lokalen Ordner auf den Geräten mit dem Cloudspeicher synchronisieren, wenn eine Internetverbindung besteht.
Für diesen Service muss man dem Cloudspeicheranbieter seine Daten anvertrauen. Natürlich kann man sich eine derartige "Cloud" auch selbst bauen,
was allerdings einen Einrichtungs- und Wartungsaufwand bedeutet.

Hier ist [Syncthing](https://syncthing.net/) eine "Cloud light". Damit kann man Dateien zwischen seinen Geräten abgleichen, ohne das man die Dateien an einer eigenen oder
fremden zentralen Stelle speichern muss. Syncthing verwendet direkte Verbindungen zwischen den Geräten, sofern es möglich ist.
Wenn sich beide Geräte hinter einer Firewall befinden, können Relay-Server verwendet werden.

---

**Inhaltsverzeichnis:**

1. [Geräte-IDs](#1-geräte-ids)
2. [Verbindungsaufbau](#2-verbindungsaufbau)
3. [Ordnerfreigabe](#3-ordnerfreigabe)
4. [Installation](#4-installation)
5. [Starten](#5-starten)
6. [Konfiguration nach dem ersten Start](#6-konfiguration-nach-dem-ersten-start)
7. [Eine Freigabe erstellen](#7-eine-freigabe-erstellen)
8. [Verwaltungsvereinfachungen](#8-verwaltungsvereinfachungen)
9. [Fazit](#9-fazit)

---


### 1. Geräte-IDs

![der Menüpunkt zum Anzeigen der Geräte-ID](/img/syncthing/id-menu.png)

![die Anzeige der Geräte ID als Text und Barcode](/img/syncthing/id-ansicht.png)

Geräte werden in Syncthing durch eine Geräte-ID repräsentiert. Diese ID ist allerdings keine zufällige Zeichenfolge, sondern der Fingerabdruck
eines Zertifikats - jede Syncthing-Installation erstellt beim ersten Start ein Schlüsselpaar. Syncthing verwendet TLS (Transport Layer Security) - das Protokoll,
das auch bei HTTPS für die Verschlüsselung verwendet wird. Im Gegensatz zu HTTPS wird allerdings keine Zertifizierungsstelle benötigt, da mit der
Geräte-ID schon eine Möglichkeit zum Prüfen der Authentizität der Gegenseite vorhanden ist und auch verwendet wird. TLS stellt hier natürlich auch
die Vertraulichkeit und Integrität der übertragenen Daten sicher.

### 2. Verbindungsaufbau

Eine theoretisch sichere Verbindung bietet keinen Mehrwert, wenn keine Verbindung zustande kommt. Hierfür hat Syncthing die konfigurationsfreie
und in der Voreinstellung aktivierte Local und Global Discovery.

Die Local Discovery erfolgt durch das Senden von UDP-Broadcasts, die die Geräte-ID und als Absender die IP-Adresse
im aktuellen Netz enthalten. In öffentlichen Netzwerken ist es also - wenn man nicht sofort wiedererkannt werden möchte - zielführend, die Local
Discovery abzuschalten. Noch besser ist es natürlich, Syncthing zu beenden, weil Syncthing bei einer eingehenden Verbindung sein Zertifikat
zeigt und damit auch indirekt die Geräte-ID preisgibt.

Die Global Discovery erfolgt über einen zentralen Server - daran führt (fast) kein Weg vorbei, wenn man durch das "Internet" eine Verbindung
aufbauen möchte. Dort meldet der Syncthing-Client, wie man ihn erreichen kann. Jede und Jeder kann diese Daten vom zentralen Server abrufen, wenn er
dort nach der Geräte-ID fragt. Die anderen eigenen Clients sollen das natürlich erfahren können, aber Dritte nicht.
Natürlich kann man auch einen eigenen Global Discovery Server betreiben und im Client hinterlegen - ob man sich die Mühe macht ist eine
andere Frage.

Wie bereits geschrieben können Relay-Server verwendet werden. Auch diese Funktion ist erst einmal eingeschaltet und wenn man diese nicht braucht
ist es besser, sie abzuschalten. Die Verbindung zum Zielgerät ist auch bei der Nutzung von Relays durch TLS geschützt, sodass der Relay-Server
keine Inhalte sehen kann. Auch die Relay-Server könnte man selbst betreiben. Eine Verbindung über einen Relay-Server wird nur versucht, wenn
keine andere Möglichkeit funktioniert. Allerdings registrieren sich Geräte - sofern aktiviert - sofort beim Relay-Server, damit den anderen Geräten
über den Global Discovery Server diese Möglichkeit angeboten werden kann.
Jetzt haben wir die Theorie für den Verbindungsaufbau zwischen den Geräten betrachtet. Eine Verbindung zu anderen Geräten ist aber langweilig,
wenn sie nicht genutzt wird. Damit kommen wir zu den Ordnern.

### 3. Ordnerfreigabe

Die freigegebenen Einheiten heißen Ordner. Innerhalb von Syncthing haben Ordner eine ID wie ``levuv-skz3b``, aber man hat die freie Wahl, wie man
sie nennen möchte - sowohl in der Syncthing-Oberfläche als auch im lokalen Dateisystem. Eine Synchronisation findet nur statt, wenn bei zwei
Syncthing-Instanzen ein Teilen dieses Ordners (identifiziert durch die Ordner-ID) mit der jeweils anderen Instanz aktiviert ist. In der Praxis
teilt man einen Ordner von einer Instanz aus - dann gibt es beim anderen Gerät einen Hinweis auf einen neuen Ordner und wenn man diesen dann
hinzufügt, dann hat man auch automatisch die richtige Ordner-ID hinterlegt.

Syncthing scannt den lokalen Ordner im Dateisystem regelmäßig und lässt sich - sofern es das eigene Betriebssystem unterstützt - über Änderungen
benachrichtigen. Intern teilt Syncthing die Dateien in Blöcke auf. Bei Änderungen werden nur die veränderten Blöcke übertragen, wodurch Syncthing
scheinbar nur die Änderungen überträgt.

### 4. Installation

Syncthing hat keine konventionelle grafische Oberfläche, sondern startet einen Webserver, über den man es konfigurieren kann.
Für Linux-Nutzer gibt es eine [native grafische Oberfläche](https://github.com/kozec/syncthing-gtk), die aber
[scheinbar nicht mehr gepflegt wird](https://github.com/syncthing/website/pull/20). Wir werden hier also die Syncthing-Weboberfläche
verwenden. Wer will kann sich die [Drittanbieter:innen-Werkzeuge für Syncthing](https://docs.syncthing.net/users/contrib.html) ansehen, die
z.B. ein Tray-Icon für Syncthing ermöglichen.

In jedem Fall muss man Syncthing installieren, bevor man es verwenden kann. Wenn man kein Linux-System verwendet oder es Syncthing nicht
in den Paketquellen gibt, dann kann man es auf der [Download-Seite](https://syncthing.net/downloads/) herunterladen. Dort gibt es auch einen Link zu
[F-Droid](https://f-droid.org/packages/com.nutomic.syncthingandroid/) - Syncthing kann man also auch unter Android verwenden.
Alternativ gibt es für Android auch [einen Fork](https://f-droid.org/en/packages/com.github.catfriend1.syncthingandroid/).

Wir verwenden zum Vorführen zwei Debian 10-Installationen. Dafür rufen wir ein Terminal auf und führen ``sudo apt install syncthing``
aus.

### 5. Starten

Zum "Starten" von Syncthing haben wir vier Möglichkeiten:

- ``syncthing`` im Terminal eingeben, damit wird Syncthing gestartet und der Standardbrowser mit der Oberfläche geöffnet
- ``syncthing -no-browser`` aufrufen (dafür gibt es in der Anwendungsliste den Eintrag "Start Syncthing"), was nur den Dienst und nicht die Oberfläche startet
- ``syncthing -browser-only`` ausführen (das verbirgt sich hinter "Syncthing Web UI"), was nur die Oberfläche im Browser öffnet und nur funktioniert, wenn der Syncthing-Dienst bereits läuft
- den Autostart aktivieren; bei Linux-Distributionen, die Systemd verwenden, reicht hierfür i.d.R. ein ``systemctl --user enable --now syncthing``; ansonsten liefert die [Dokumentaion](https://docs.syncthing.net/users/autostart.html?highlight=autostart) Anweisungen für verschiedene Systeme

Wie man an die Oberfläche gelangt ist eine individuelle Entscheidung. Wenn man einen Wrapper wie SyncTrayzor verwendet, dann hat man andere Möglichkeiten dafür.

### 6. Konfiguration nach dem ersten Start

Nach dem Aufrufen der Oberfläche erhält man die Frage, ob man die Telemetrie-Funktion aktivieren möchte. Danach sollte man im Menü oben rechts "Aktionen" und dann "Einstellungen" wählen.

![der Bereich der Syncthing-Einstellungen, in dem eingestellt werden kann, wie die Oberfläche erreichbar sein soll](/img/syncthing/gui-einstellungen.png)

In der Voreinstellung ist die Oberfläche nur über ``127.0.0.1:8384`` erreichbar. ``127.0.0.1`` ist eine der Adressen des sogenannten Loopback-Interfaces -
auf die Oberfläche kann also nur vom dem Rechner, auf dem Syncthing läuft, zugegriffen werden. Wir empfehlen dennoch ausdrücklich, unter "GUI", einen
Nutzernamen und ein Passwort für die Nutzeroberfläche festzulegen. Solltet ihr eine virtuelle Maschine mit dem QEMU User Mode Networking verwenden, dann
kann das Gastsystem unter ``10.0.2.2`` das Loopback-Interface eures Wirtes und somit Syncthing erreichen und beliebig steuern, was die Isolation durch die
Virtualisierung wieder aushebeln würde. Ihr würdet auch eure Benutzerverwaltung aushebeln, weil auch der extra unpreviligierte Benutzer nobody Syncthing steuern könnte
und damit auf alle Dateien zugreifen kann, auf die auch ihr zugreifen könnt. **Also**: Passwort setzen, wir haben es euch gesagt.

![der Bereich der Syncthing-Einstellungen, in dem die Verbindung zu anderen Geräten konfiguriert werden kann](/img/syncthing/verbindungs-einstellungen.png)

Dann gibt es noch die Verbindungs-Einstellungen. Die "NAT-Durchdringung" meint hier, dass Syncthing versucht, bei eurem Router eine Portweiterleitung zu
konfigurieren, damit eine Verbindung ohne Relay-Server ohne weitere Konfiguration möglich wird. In dem meisten Fällen ist das nur sinnvoll, wenn auch die
"Globale Gerätesuche" (die Global Discovery) aktiviert ist. Die "Lokale Gerätesuche" ist entsprechend die Local Discovery und die "Weiterleitungen" meint
die Relay-Server. Wenn man sich bei einer Option sicher ist, dass man das nicht braucht, dann sollte man diese abschalten.

![die Syncthing-Oberfläche mit dem Default Folder und ohne hinzugefügte Geräte](/img/syncthing/frontend.png)

### 7. Eine Freigabe erstellen

Die Übersichtsseite sieht noch sehr leer aus - es gibt zwar einen voreingestellten Ordner, aber keine Geräte, mit denen man diesen teilen könnte.
Der "Gerät hinzufügen"-Button ändert das. Dabei braucht man die Kennung des anderen Gerätes - also die Geräte-ID. Die findet man beim anderen Gerät
im Menü. Das Menü und die Anzeige von der Geräte-ID haben wir vorhin schon einmal gezeigt.
Wenn die Local Discovery zwischen beiden Geräten funktioniert, dann gibt es im Hinzufügen-Dialog unter dem ID-Eingabe-Feld Vorschläge, die man
direkt anklicken kann, um den entsprechenden Wert einzufügen. Bei der Android-Version von Syncthing (und evtl. auch bei anderen Wrappern) kann
man auch einen Barcode, der eine Geräte-ID enthält, scannen.

Wichtig ist, einen Ordner (z.B. den "Default Folder") mit dem Gerät zu teilen, weil sonst
kein Verbindungsaufbau versucht wird. Die Freigaben können mit der Bearbeitung vom Gerät oder mit dem Bearbeiten vom Ordner erstellt werden - in beiden
Fällen ist der Inhalt vom Tab "Teilen" der Ort, bei dem man es konfigurieren kann.

![Syncthing-Oberfläche mit einem Hinweis auf einen Verbindungsversuch von einem unbekannten Gerät](/img/syncthing/neues-geraet.png)

Das zweite Gerät kennt das erste nicht. Nun könnte man die gleichen Schritte am anderen Gerät wiederholen - aber es geht auch einfacher. Wenn sich ein
unbekanntes Gerät meldet, können wir angeben, dass das erwünscht ist und wir dieses Gerät hinzufügen wollen. Das erste Gerät erfährt aber nichts hiervon
und solange am zweiten Gerät nichts freigegeben wird, versucht es nicht, eine Verbindung zum ersten Gerät aufzubauen. Ein Neustart am ersten Gerät kann
dieses Problem schnell lösen. Dann kommt die nächste Frage:

![Syncthing-Oberfläche mit der Meldung, dass ein Gerät einen Ordner mit uns teilen möchte](/img/syncthing/neuer-ordner.png)

Nach einer zweiten Bestätigung ist auch dieses Problem gelöst und die Verbindung mit Synchronisation steht. Der "Default Folder" ist der Unterordner
"Sync" im Home-Verzeichnis. Wenn eine Datei in einen Sync-Ordner gelegt wird, dann taucht diese zeitnah im anderen Sync-Ordner auf.

![der Ordner-Hinzufügen-Dialog von Syncthing](/img/syncthing/ordner-hinzufuegen.png)

Natürlich kann man auch andere Ordner freigeben. Dafür muss man im "Ordner hinzufügen"-Dialog den lokalen Pfad angeben. Hier wird z.B. der Dokumente-Ordner
geteilt. Für jeden Ordner können andere Geräte gewählt werden, mit denen dieser geteilt wird.

### 8. Verwaltungsvereinfachungen

Wenn ein Ordner zwischen drei oder mehr Geräten geteilt wird, dann wird das Hinzufügen eines weiteren Gerätes
zum Cluster aufwändig, wenn jeder direkt mit jedem synchronisieren soll - es müssten alle bestehenden Geräte
manuell mit dem neuen Gerät gekoppelt werden.

Hier bietet Syncthing die Möglichkeit, auf dem neuen Gerät nur ein Gerät zu hinterlegen und dieses als "Introducer" bzw. Verteiler festzulegen.
Dann werden alle weiteren Geräte, mit denen der Introducer die gemeinsamen Ordner teilt, automatisch hinzugefügt.

Zur Veranschaulichung ein Beispiel: Das neue Gerät X wird für einen Sync des Ordners "Notizen" bei dem Gerät A hinterlegt.
Gerät A teilt "Notizen" auch mit den Geräten B und C. Wenn nun das neue Gerät X das Gerät A als Verteiler festlegt und die
Ordnerfreigabe akzeptiert, wird beim Gerät X durch das Gerät A automatisch das Gerät B und C hinzugefügt und auch der Ordner
mit diesen Geräten abgeglichen.

### 9. Fazit

Syncthing ist also eine Lösung, um Dateien auf mehreren Geräten abzugleichen, ohne das man eine Zentrale benötigt. Die Einrichtung benötigt etwas Zeit, aber
danach kann man es starten und muss nicht weiter darüber nachdenken. Dank der Local Discovery wird dabei nicht einmal eine Internetverbindung benötigt. Es
reicht sogar schon, die Netzwerkkarten von zwei Geräten mit einem Netzwerkkabel zu verbinden - durch die Link-Local-Adressen erhalten beide Geräte eine IP-Adresse
und die "Broadcasts" für die Local Discovery funktionieren in diesem Fall sowieso. Dabei ist darauf zu achten, dass der NetworkManager nicht die Verbindung abbaut -
bei einem Test hat er nach einer kurzen Zeit ohne Antwort auf seine DHCP-Anfragen aktiv die Verbindung getrennt und damit auch die Link-Local-Verbindung abgebaut.
Nach dem Einstellen, dass nur eine Link-Local-Adresse gewünscht ist, trat dieses Verhalten nicht mehr auf - aber man darf nicht vergessen, diese Einstellung wieder
rückgängig zu machen, weil man sonst keine Internetverbindung mehr aufbauen kann.

### Literatur und Quellen

- <https://syncthing.net/>
- <https://docs.syncthing.net/users/security.html>
- <https://docs.syncthing.net/dev/device-ids.html>
- <https://docs.syncthing.net/specs/localdisco-v4.html>
- <https://docs.syncthing.net/specs/globaldisco-v3.html>
- <https://docs.syncthing.net/users/relaying.html>
- <https://docs.syncthing.net/specs/bep-v1.html>
- <https://docs.syncthing.net/users/syncing.html>
- <https://docs.syncthing.net/users/introducer.html>
- <https://forum.syncthing.net/t/functionality-of-introducers/8058>
. <https://github.com/syncthing/syncthing/issues/1819>
