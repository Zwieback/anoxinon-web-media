+++
title = "Datenschutz in der Vermietung"
date = "2019-06-26T21:00:00+02:00"
tags = ["Anfänger"]
categories = ["Datenschutz"]
banner = "/img/thumbnail/vermietung_coloriert.png"
description = "Mieterhöhung per Whatsapp? Abfrage der Blutgruppe des Kindes? Gut, letzteres war ein kleiner Scherz. In dem heutigen Beitrag möchten wir aber auf Probleme im Bereich der Vermietung hinweisen und entsprechende Lösungsansätze präsentieren."
introblock = "Trotz sorgfältiger Recherche behalten wir uns Irrtümer vor. Dies ist keine Rechtsberatung. Bei juristischen Fragen wenden Sie sich bitte an einen fachkundigen Anwalt."
+++

Beiträge über Datenschutz im Mietverhältnis sind bislang schwer zu finden. Dennoch sollte jeder Vermieter spätestens nach Inkrafttreten der Datenschutzgrundverordnung (DSGVO) in diesem Bereich ausreichend geschult sein. Mit diesem Artikel wollen wir bei Mietern und Vermietern ein Bewusstsein für den Datenschutz schaffen und entsprechende Handlungsempfehlungen geben.

---

**Inhaltsverzeichnis:**     
[I. Was Vermieter in Sachen Datenschutz beachten sollten](#Vermieter_Datenschutz_Beachten)    
[II. Auf Wohnungssuche](#Auf_Wohnungssuche)  
[III. Die Sache mit der SCHUFA Auskunft](#SCHUFA)    
[IV. Datenschutz, wenn Sie bereits Mieter sind](#Datenschutz_Mieter)    
[V. Sie möchten ausziehen?](#Ausziehen)    
[VI. Weitere Fragen](#Weitere_Fragen)    
[VII. Handlungsempfehlung für Vermieter](#Handlungsempfehlungen)    
[VIII. Schlussworte](#Schlussworte)    

---

### I. Was Vermieter in Sachen Datenschutz beachten sollten<a name="Vermieter_Datenschutz_Beachten"></a>

Private Vermieter nutzen zur Kommunikation mit dem Mieter bzw. Mietinteressenten nicht selten ihre **privaten** Kommunikationswege. So haben sie häufig keine gewerbliche Enterprise Version von Windows 10. Das bedeutet: Noch weniger Privacy-Einstellungsmöglichkeiten, als Windows 10 ohnehin schon bietet. Hinzu kommt, dass sie gerne ihr privates Smartphone verwenden und daher auch die Mieter mittelbar in das Telefonbuch - auf Apps wie Facebook  oder Whatsapp - eingespeist werden oder die Kommunikation sogar direkt über diese Dienste oder über Email-Konten bei Yahoo, Google, Microsoft & Co. stattfindet.

Hieran ist nicht nur problematisch, dass Mieter faktisch gezwungen werden, sich auf derart datenhungrige Dienste einzulassen (bekanntlich besteht ihr Geschäftsmodell in der Kommerzialisierung von Nutzerdaten für Werbezwecke), denn vor allem bedeutet aber der Firmensitz dieser "Datenkraken" in den USA, dass der Datenschutz, wie wir diesen in Europa kennen, nicht eingehalten wird.

Der  [CLOUD Act](https://www.orrick.com/Insights/2018/04/The-CLOUD-Act-Explained) ermöglicht den USA auch weiterhin und trotz DSGVO, auf Server von US-Unternehmen im europäischen Ausland zuzugreifen. Zudem verhandeln [EU und USA darüber, diesen Zugriff zukünftig noch einfacher zu gestalten](https://netzpolitik.org/2019/eu-startet-gespraeche-mit-den-usa-ueber-zugriff-auf-cloud-daten/).

Doch die Krönung ist, dass einige Privatpersonen zurzeit verstärkt auf Fintechs setzen. Eingedeutscht handelt es sich dabei um sogenannte Zahlungsauslöse- oder Kontoinformationsdienste i.S.d. [§ 675f Abs. 3 BGB](https://dejure.org/gesetze/bgb/675f). Erstere werden zwischengeschalten um Zahlungen auszulösen. Bekannte Zahlungsauslösedienste sind z.B. Paypal, Stripe, Sofortüberweisung und Paydirekt.
Letztere scannen und analysieren das gesamte Konto inkl. Ihrer Transaktionen sowie Ihren Kontostand um zusätzliche Informationen über die eigene Kontonutzung, z.B. eine genaue Analyse des eigenen Zahlungsverhaltens, zu liefern. Jedoch ist der Preis für diesen Komfort eine Aushöhlung der Privatsphäre für Werbezwecke.

Davon sind natürlich auch die eingehenden Zahlungen der Mieter betroffen (Ihre BLZ, Kontonummer, Beitrag, Name, Verwendungszweck). Leider ist dies durch ([PSD2](https://de.wikipedia.org/wiki/Zahlungsdiensterichtlinie)) seit kurzem legal - damit wurde das Bankgeheimnis aufgehoben. Umgesetzt in § 675f Abs. 3 BGB kann der Vermieter sich frei für die Nutzung von solchen Diensten entscheiden. Auf diese Weise soll der Wettbewerb zwischen Zahlungsdiensten angekurbelt werden.

Wenn Sie Ihr Mietobjekt schließlich auf Websiten publik machen (z. B. Immoscout, Immowelt & Co.), stellen Sie bitte sicher, dass es auch alternative Kontaktmöglichkeiten gibt, wenn man den Datenschutzbestimmungen des Immobilienportals nicht zustimmt. Beachten Sie auch, wenn Sie eine Handynummer angeben, dass diese nicht mit Whatsapp und Co. verknüpft sein sollte. Whatsapp macht nämlich in seiner Datenschutzrichtlinie deutlich, dass die praktizierte Übermittlung von Daten aus dem Adressbuch eine [vorherige Einwilligung](https://www.anwalt.de/rechtstipps/whatsapp-und-facebook-datenuebermittlung-auch-ohne-einwilligung_088303.html) **aller** dort gespeicherten Kontakte voraussetzt. Hier sollten auch die Immobilienportale eine Aufklärungspflicht gegenüber Inserierenden wahrnehmen und so potentielle Mieter schützen. Ob eine Nummer Whatsapp nutzt, ist immerhin ohne großen Aufwand überprüfbar.

Als Vermieter sollten Sie sich bewusst sein, dass bei komplexen und längerfristigen Rechtsbeziehungen, wie dem Mietvertrag zwangsläufig Daten von Dritten mitverarbeitet werden bzw. auf Drittanbieter zurückgegriffen wird (was in aller Regel erneut die Einwilligungspflicht nach Art. 6 Abs. 1 lit. a, 7 DSGVO auslöst). Es liegt daher in Ihrer Verantwortung und Ihrem Interesse, diesen Drittbezug auf ein Minimum zu reduzieren.

---

### II. Auf Wohnungssuche<a name="Auf_Wohnungssuche"></a>

Wenn Sie heutzutage auf Wohnungssuche sind, müssen Sie einiges über sich ergehen lassen. Der Wohnraum ist knapp, die Mieten hoch und Sie stehen in allseitigem Konkurrenzkampf.  Viele Vermieter nutzen diese Situation aus und fordern von Ihnen Dinge, die Sie so nicht fordern dürften - im Wissen, dass Sie nahezu
"machtlos" sind. Der Vermieter darf nur Fragen stellen, an denen er ein **berechtigtes Interesse** i.S.v. Art.6 Abs.1 lit.f DSGVO hat bzw. die zur Durchführung des Mietverhältnisses erforderlich sind. Unzulässige Fragen müssen nicht vom zukünftigen Mieter beantwortet werden. Das Recht auf informationelle Selbstbestimmung (Art. 2 Abs.1, 1 Abs.1 GG), wie es durch die DSGVO ausgestaltet ist, muss vom Vermieter berücksichtigt werden.

Bei unseren Recherchen sind uns auch nach Einführung der DSGVO einige Interessentenbögen aufgefallen, welche die DSGVO nicht berücksichtigt haben und weshalb die Vermieter im Ernstfall mit sehr hohen Bußgeldern rechnen müssen.

Einen davon nehmen wir als Beispiel und erklären Stück für Stück, **warum viele der nachfolgenden Fragen nach unserer Auffassung und der von Experten unzulässig sind**.

---

![](/img/vermietung/wohnung.png)

---

**Angaben zum Beziehungsstatus (verheiratet/verwitwet/getrennt/ledig):** Hier besteht **kein** berechtigtes Interesse des Vermieters. Meist  werden diese Angaben vom Vermieter im Hinblick auf die gesamtschuldnerische Haftung von Ehegatten erfragt. Daran besteht jedoch kein berechtigtes Interesse, da der andere Ehepartner nicht zwingend Teil des Mietvertrages wird. Der Beziehungsstatus darf daher nicht erfragt werden.

**Staatsangehörigkeit:** Auch an der Staatsangehörigkeit besteht **kein** berechtigtes Interesse (Art. 6 Abs. 1 DSGVO). Nicht nur aus Sicht des Datenschutzes ist diese Abfrage kritisch zu bewerten, da man Menschen auf Grund der Rasse, der ethnischen Herkunft und der Religion bei der Vermietung vom Wohnraum nicht unterschiedlich behandeln soll. Dies ergibt sich aus § 19 Abs. 1, Abs. 3 AGG.

**Telefon dienstlich:**: Nicht jeder darf während der Arbeit telefonieren bzw. wurde ggf. das Telefonieren
aus privaten Zwecken untersagt.

**Name des Arbeitgebers:** Hier besteht kein berechtigtes Interesse. Ein Kontoauszug der letzten Monate sollten für einen Nachweis über ein festes Einkommen ausreichen.

**Beim Arbeitgeber beschäftigt seit:** Hier besteht kein berechtigtes Interesse. Ein Kontoauszug der letzten Monate sollten für einen Nachweis über ein festes Einkommen ausreichen. Denn aus der Dauer des Arbeitsverhältnisses können - anders als bei der Information nach dem Bestehen eines Arbeitsverhältnisses - keine validen Rückschlüsse auf die relevante Zahlungsfähigkeit geschlossen werden.

**Angaben zum Partner:** Angaben zum Partner sind unzulässig, da dieser nicht zwingend Teil des Mietvertrages ist. Bei Angaben zum Partner müsste gekennzeichnet sein, dass es sich um einen Vertragspartner handelt, welcher auch den Mietvertrag unterzeichnet.

---

![](/img/vermietung/wohnung2.png)

---

**Einziehende Kinder:** Der Vermieter darf auch **keine** personenbezogenen Angaben über die einziehenden Kinder verlangen. Diese sind ebenfalls nicht Teil des Mietvertrages. Angaben wie Namen, Arbeitgeber der Kinder, ihr Einkommen sowie Geburtstag und Nationalität dürfen nicht erfragt werden. Hier besteht kein berechtigtes Interesse. Erfragt werden darf lediglich die Anzahl der einziehenden Personen.

---

![](/img/vermietung/wohnung3.png)

---

**Einkünfte des Partners und/oder der Kinder:** Einkünfte des Antragsstellers durch Kindergeld haben den Vermieter grundsätzlich nicht zu interessieren. Es müsste eine Zustimmung des Mieters zur Offenbarung geschützter Angaben vorliegen. [Auch ist grundsätzlich keine Zahlung des Kindergeldes an Dritte möglich.](https://www.frag-einen-anwalt.de/Abtretungserklaerung-Kindergeld-an-den-Vermieter--f285635.html).

**Angaben über die jetzige Wohnung (Zimmer, Miete, Bewohnt seit):**                     
Diese Auskunft ist dem Mieter grundsätzlich nicht zumutbar. Sie würde lediglich den Vermieter darüber informieren, wie hoch die Vormiete war. Die daraus resultierende Bonität des Mieters ergibt sich aber bereits aus anderen Auskünften wie Kontoauszügen oder dem monatlichen Bruttoeinkommen. Es fehlt daher an der datenschutzrechtlichen Erforderlichkeit.

**Name bzw. Kontaktdaten des jetzigen Mieters/Vormieters:**
Eine Vorvermieterbescheinigung muss nicht vorgelegt werden (BGH vom 30.09.2009 Az. VII ZR 238/08). Der frühere ist daher gegenüber dem neuen Vermieter nicht verpflichtet, eine solche Bescheinigung auszustellen. Ein geschwärzter Kontoauszug zur Bescheinigung, dass der Mietinteressent / zukünftige Mieter auch regelmäßig seine Miete überwiesen hat, sollte genügen.
Notfalls ist der Vermieter verpflichtet, dem Ex-Mieter eine Quittung über die letzten 3 Jahre auszustellen (§ 368 BGB).

**Vorstrafen und strafrechtliche Ermittlungsverfahren:**
Ist zwar nicht Teil unseres "Muster"-Beispiels, jedoch wird dies ebenfalls oft zu Unrecht erfragt.
Berücksichtigt werden muss zum einen, dass bestimmte Strafen nicht in ein polizeiliches Führungszeugnis aufzunehmen sind, § 32 Abs. 2 BZRG, und sich schon deshalb keine darüber hinaus gehenden Mitteilungspflichten gegenüber einem Vermieter ergeben können. Wenn man z.B. sich bei der Polizei bewirbt, ist eine Abfrage der Vorstrafen zentral, da die besondere Sensibilität von Gefahrenabwehr und Strafverfolgung besondere Anforderungen an die Zuverlässigkeit der Bewerber stellt. Bei der Anbahnung von Mietverhältnissen besteht keine vergleichbare Gefährdungslage, da  hier ausschließlich die  Frage  nach  der  Bonität der Mietinteressenten von Bedeutung ist. Auch die  Erhebung  von  Informationen  zu  laufenden  strafrechtlichen  Ermittlungsverfahren ist unzulässig.

---

![](/img/vermietung/wohnung4.png)

---

**Passkopie:**
Nach der alten Rechtslage, die bis zum 14.07.2017 galt, war das Scannen oder elektronische Abspeichern  von Ausweisen (z.B. als Pdf) grundsätzlich unzulässig. Die Rechtslage hat sich inzwischen zwar verändert (§ 20 PAuswG), jedoch kann der Mietinteressent immer noch Ausweiskopien widersprechen.

Dabei sollte er insbesondere darauf hinweisen, dass ein einmaliger Kontrollblick auf den Ausweis genügt, so kann der Vermieter die persönlichen Daten auf der Selbstauskunft oder Wohnungsgeberbestätigung überprüfen.
Der Vermieter darf somit auf keiner Personalausweiskopie bestehen.

Abschließend gilt: Bei denjenigen Mietinteressenten, die die Wohnung nicht erhalten haben, entfällt der legitime Zweck der Datenverarbeitung (Art. 5 Abs. 1 lit. b und e DSGVO). Ihre personenbezogenen Daten dürfen daher nicht gespeichert werden.

---

### III. Die Sache mit der SCHUFA Auskunft<a name="SCHUFA"></a>

Bei Mietverträgen handelt es sich um zumeist langfristige vertragliche Bindungen. Um das zukünftige Nutzungsverhalten und die Zahlungsfähigkeit des künftigen Mieters einzuschätzen, ist ein Vermieter daran interessiert, möglichst viele und genaue Auskünfte über seinen Vertragspartner zu erlangen. In der Praxis gelangt er über eine Wirtschaftsauskunftei an diese Informationen zur Bonität.

Die Schufa (Schutzgemeinschaft für allgemeine Kreditsicherung) ist die [größte Wirtschaftsauskunftei Deutschlands](https://www.tagesspiegel.de/wirtschaft/schufa-und-co-was-auskunfteien-ueber-verbraucher-wissen/11624756.html). Sie hat daher viel Macht auf die Wirtschaftlichkeit einzelner. Die Schufa (und viele andere Auskunfteien) agieren jedoch im Dunklen. Niemand weiß, **wie** die Scores ermittelt werden, von denen die Wirtschaftlichkeit vieler abhängt - so gesehen kann sich auch kaum einer zu 100% schützen. Ein paar Indikatoren, wie z. B. das bei der Berechnung auch der [Wohnort](https://financer.com/de/finanztipps/schufa-score-verbessern/) ermittelt wird, gibt es jedoch.
Zieht man beispielsweise in ein schlechteres [Wohnviertel/Ghetto](https://www.welt.de/finanzen/verbraucher/article13255357/Wer-am-falschen-Ort-wohnt-bekommt-keinen-Kredit.html) obwohl man damit über seine finanziellen Verhältnisse lebt und somit eigentlich positiv bewertet werden sollte, wird man negativ bewertet, denn Zahlen wie das [Gehalt](https://www.mystipendium.de/geld/schufa-score) zählen nicht zu den Berechnungswerten.

Viele Daten werden von Vertragspartnern weitergeleitet. Die Schufa selbst gibt an, die weitergeleiteten Daten nicht auf Richtigkeit zu überprüfen, sondern direkt in die Datenbank aufzunehmen. Durch die enormen Mengen an Daten, die die Schufa täglich verarbeitet, ist es also nicht verwunderlich, dass viele Daten veraltet oder unrichtig sind. Bei "[Achtung Abzocke](https://www.kabeleins.de/tv/achtung-abzocke)" wurde erst kürzlich ein Fall bekannt, wo eine Dame durch den Umzugsservice der Schufa unverschuldet in Bedrängnis gekommen ist. Dieser Service bietet Unternehmen an, dass die Schufa bei ausstehenden Forderungen des Unternehmens gegen eine nicht mehr auffindbare Privatperson haben diese in ihrer "Auskunftei" sucht. Im Beispiel hatte die Frau jedoch einen in Deutschland populären Nachnamen und die Schulden wurden einer fremden Person zugeordnet. Das Opfer wendete sich mehrmals an die Schufa, konnte aber den Score aus eigener Kraft nicht verbessern. Es gibt viele ähnliche Fälle, wo Menschen unverschuldet in diese Lage kamen.

Die Qualität des Schufa-Datensatzes steht daher nicht unberechtigt in der Kritik. Es gibt jedoch [Hinweise](https://www.check24.de/girokonto/news/schufa-beim-girokonto-was-verbraucher-wissen-sollten-65402/), dass auch mehr als eine [Kreditkarte](https://finanzkun.de/artikel/wie-berechnet-sich-der-schufa-score/), mehr als ein [Girokonto](https://www.financescout24.de/wissen/ratgeber/7-tipps-verbesserung-kreditwuerdigkeit) oder auch mehrere Schufaabfragen in kürzester Zeit zu einem negativen Ergebnis führen können. Wenn viele Vermieter das nun anfordern, alleine schon bei der Besichtigung, kann sich ggf. der Schufa Score der Interessenten unverschuldet verschlechtern.

Die [OpenSchufa](www.openschufa.de) versucht/e seit 2018 den Algorythmus der Schufa zu entschlüsseln, was ein lobenswertes Ziel ist. Jedoch war es für diese Aktion ebenfalls notwendig große Datenspenden dafür zu erbringen, welche ebenfalls sehr sensible Daten enthalten haben.

Allgemein sollten Vermieter daher besser keine Schufa-Artikel verlangen. Zudem darf eine Bonitätsauskunft auch nicht ohne explizite Einwilligung eingeholt werden; ein Mietvertrag, der keinen entsprechenden Passus enthält, ist keine explizite Einwilligung. Des Weiteren kann auch nur ein Unternehmen eine Schufa-Auskunft einholen, ein Privatvermieter nicht. Diese können die Bonitätsauskunft "lediglich" von den Interessenten/Mietern erhalten. Wir würden aus Datenschutzgründen  dazu raten, lieber auf geschwärzte Kontoauszüge zu setzen und im Notfall eine Schufa-Auskunft speziell für Vermieter zu beantragen. Niemals sollten Sie Ihre ausführliche Schufa-Auskunft Ihrem Vermieter zugänglich machen, auch nicht geschwärzt!

Erst recht darf der Vermieter keine weitergehenden Auskünfte im Sinne des Art. 15 DSGVO über den Mieter ohne dessen Mitwirkung einholen. Grund hierfür ist, dass solche Auskünfte oft nicht nur über die Vermögensverhältnisse, sondern auch die Lebensumstände im Allgemeinen Aufschluss geben.

---

### IV. Datenschutz, wenn Sie bereits Mieter sind<a name="Datenschutz_Mieter"></a>

Aber auch wenn Sie bereits Mieter sind, haben Sie einige Rechte. Wer kann unter welchem Umständen Daten erhalten, wenn Sie bereits Mieter sind?

**Handwerker:**
Wenn ein Handwerker Daten des Mieters elektronisch verarbeitet - und sei es "nur" Name oder Telefonnummer -, muss ein Auftragsverarbeitungsvertrag zwischen Vermieter und Handwerksbetrieb geschlossen sein (Art. 28 Abs. 3 DSGVO). Der Handwerksbetrieb verpflichtet sich damit auf die  Vorgaben der DSGVO in dem vertraglich festgelegten Rahmen (v.a. Dauer, Gegenstand und Zweck der Datenverarbeitung). Konkret sollte der Handwerker also weder Daten auf dem privaten Smartphone speichern noch aus den oben genannten Gründen Whatsapp & Co. (auch wenn es leider gängig ist) auf seinem Endgerät installiert haben. Er sollte außerdem eine dem Umfang seines Gewerbes entsprechende, ausreichend geschützte IT-Infrastruktur sicherstellen (Art. 32 DSGVO, [hier ausführlich](https://easycontracts.de/der-auftragsverarbeitungsvertrag-was-ist-das-und-wozu-braucht-man-das/) zusammengestellt). Nachdem er die Handwerksarbeiten beendet hat,  hat der Handwerker sodann sämtliche Daten zu löschen (Art. 28 Abs. 3 S. 1 lit. g DSGVO). Dies wäre bei Notizen auf Papier anders, deren (zeitweise) Aufbewahrung wohl gängige Praxis ist.

Was ist aber, wenn der Handwerker trotzdem Whatsapp & Co nutzt oder anderweitig gegen die DSGVO verstößt und meine Daten dadurch in Gefahr sind?

Grundsätzlich kann der Vermieter jederzeit überprüfen, ob der Handwerksbetrieb sich an die DSGVO hält. Wie oft das in der Praxis geschieht, ist uns leider nicht bekannt - auch kann es sein, dass Vermieter und Handwerksbetrieb ggf. langjährige Geschäftsbeziehungen oder gar Freundschaften pflegen und dies dann mal gerne "unter den Tisch kehren". Denjenigen sei gesagt, dass sie (in dem Falle der Vermieter) i.d.R. gesamtschuldernisch mit dem Handwerksbetrieb für sämtliche materielle und immaterielle Schäden haften (Art. 82 Abs. 4 DSGVO).  

**Heizungsableser:**

Hier gilt dasselbe wie bei Handwerkern. Eine explizite Einwilligung ist notwendig.

**Muss man Feuermelder mit Mikrofon hinnehmen?:**

Grundsätzlich gilt hier: § 201 Verletzung der Vertraulichkeit des Wortes: Mit Freiheitsstrafe bis zu drei Jahren oder mit Geldstrafe wird bestraft, wer unbefugt das nichtöffentlich gesprochene Wort eines anderen auf einen Tonträger aufnimmt oder eine so hergestellte Aufnahme gebraucht oder einem Dritten zugänglich macht.

**Nebenkostenabrechnungen:**
Hier gilt dasselbe wie bei Handwerkern. Eine explizite Einwilligung ist notwendig.

Auch dürfen die Rechnungen/Abrechnungen generell nur mit gesonderter Einwilligung per E-mail übermittelt werden.

---

### V. Sie möchten ausziehen?<a name="Ausziehen"></a>

Beim Auszug des Mieters stellt sich zunächst die Frage nach Fotos für die Weitervermietung. In die Anfertigung solcher Fotos durch den Vermieter muss der Mieter, dessen Kernbereich persönlicher Lebensführung betroffen ist, natürlich nicht einwilligen (anders bei Besichtigungen, die man als Mieter grundsätzlich zu dulden hat). Zudem übt der Mieter das Hausrecht in der Wohnung (noch) aus und darf also auf seiner Anwesenheit bei Aufnahme der Fotos bestehen.
Bedenken Sie, dass Fotos Metadaten (Exifdaten) enthalten, die nicht nur Angaben zur Kamera liefern, sondern auch zum Aufnahmedatum und zum Standort (GPS). Vor Veröffentlichung von Fotos sollten Mieter als auch Vermieter prüfen, [dass die Exif-Daten entfernt wurden.](https://wiki.ubuntuusers.de/ExifTool/)

Was für Vermieter hier wichtig ist: geben Sie keine Daten über den jetzigen Mieter an die Interessenten oder Nachmieter weiter. Wenn die Wohnung derzeit noch belegt ist, können Sie sich vor der Wohnung oder am besten einem neutralen Ort treffen und dort gemeinsam hinfahren. Unerlaubt ist, dass Daten ohne explizite Einwilligung weitergegeben werden. Dies zählt auch, wenn ein Nachmieter einen Internetanschluss einrichten möchte und dafür die TAE-Dosen Nummer oder den Namen braucht. Sie sollten immer die TAE-Nr. weitergeben, da diese nicht personenbezogen ist. Der Vermieter ist erstens nicht befugt, diese Daten mündlich oder schriftlich (gerade elektronische Kommunikation ist generell riskant) ohne Einwilligung zu übermitteln und zweitens hat der Nachmieter keine Erlaubnis die Daten des Vorbesitzers an den Telekommunikationsdienstleister seiner Wahl weiterzugeben.

Des Weiteren kann meist auch ohne TAE-Dosen Nummer oder Namen des Vormieters, nämlich alleine durch die Angabe der Straße und des Stockwerks der Internetanschluss korrekt zugeordnet werden.

Fazit: Geben Sie als Vermieter also **keine Daten über ehemalige/derzeitige Mieter an künftige Mieter weiter**. Dazu zählt auch der Name, auch nicht wenn der neue Mieter einen Internetanschluss einrichten möchte und "angeblich" dafür den Namen benötigt. Die Internetanbieter können den Anschluss anhand der TAE-Dosennummer zuordnen (auch wenn [diese oft mehr Daten abfragen wollen](https://hilfe-center.1und1.de/dsl-c82789/umzug-c84046/angabe-der-vormieterdaten-beim-dsl-umzug-a785458.html), als eigentlich benötigt oder datensparsam nach DSGVO wären) oder allein auf Grund welchen Stockwerkes in welcher Straße Sie wohnen. Ein Name des Vormieters ist hierfür nicht notwendig, des weiteren liegt in der Regel **keine explizite Einwilligung** (DSGVO, Art. 6 Rechtmäßigkeit der Verarbeitung) vor, dass der Vermieter diese Daten an den Nachmieter weiterleiten darf und auch kein berechtigtes Interesse.

---

![](/img/vermietung/vermietung_1und1.png)

---

**Wohnungsgeberbestätigung**  
Eine Meldebescheinigung, die durch die Gemeinde ausgestellt wird, beinhaltet Daten darüber, wo und seit wann jemand an einer bestimmten Adresse gemeldet ist. Daten wie Vor- und Zuname, Geburtsdatum, Familienstand, Staatsangehörigkeit sowie die aktuelle Anschrift der Hauptwohnung und ggf. auch der Nebenwohnung werden dort angegeben. Seit kurzem möchte die Meldebehörde zusätzlich eine Wohnungsgeberbestätigung vom Vermieter erhalten. Begründung: Es soll verhindern, dass Straftäter ihren Aufenthaltsort verschleiern können. Vorlagen hierfür können meist auf der Homepage der Stadt heruntergeladen werden. Wir empfehlen, da auch Personen eingetragen werden, die nicht Teil des Mietvertrages sind, diese lediglich herunterzuladen, auszufüllen und eine Kopie nicht-digital abzuspeichern.

---

### VI. Weitere Fragen<a name="Weitere_Fragen"></a>

**Was darf man rechtlich tun, wenn man merkt das ein Vermieter unzulässig handelt?**
Als Mietinteressent: Lassen Sie sich am besten alles schriftlich geben, sollten Sie einen Interessentenbogen mit "unzulässigen" Fragen haben, bzw. sich benachteiligt sehen, weil Sie manche Fragen nicht beantworten möchten senden Sie diesen Interessenbogen an Ihren Datenschutzbeauftragten.

Kontaktdaten der Datenschutzbeauftragten (nach öffentlicher / nicht-öffentlicher Bereich sowie Bundesland sortiert): [Anschriftenliste](https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html).

Diese Kontaktmöglichkeit können Sie bei Unternehmen, die Wohnungen zur Vermietung anbieten als auch bei [Privatvermietern](https://remax-kontor.de/dsgvo-betrifft-auch-privatvermieter/) verwenden, wenn diese gegen den Datenschutz verstoßen.

Sollten Sie keinen direkten Interessentenbogen haben oder bei anderen Anfragen stutzig werden, wie etwa der Anfrage einer ungeschwärzten vollständigen Schufa-Auskunft (die eigentlich nur für Sie gedacht ist) oder der Kopie (ggf. ungeschwärzt) Ihres Personalausweises lassen Sie sich diese Anfrage schriftlich geben! Im Zweifel können Sie dies ebenfalls der Datenschutzbehörde zugänglich machen.

**Wann müssen Unterlagen gelöscht werden?**
Bei Mietinteressenten: Unmittelbar, wenn feststeht, dass diese die Wohnung nicht bekommen und ihnen das Ergebnis mitgeteilt wurde, müssen diese vollständig vom Vermieter gelöscht/vernichtet werden.
Bei ehemaligen Mietern: Hier müssen Unterlagen aus steuerrechtlichen Gründen länger aufbewahrt werden. Es besteht eine handels- und steuerrechtliche Speicherpflicht von 10 Jahren für bestimmte Daten (vermietetes Objekt, Miete etc.).

---

### VII. Handlungsempfehlung für Vermieter<a name="Handlungsempfehlungen"></a>

* Verlangen Sie keine ausführliche Biografie mit viel zu privaten Informationen (z. B. Name der Kinder, deren Geburtstag etc.) oder welchen Familienstand Ihr Mieter hat, auch eine Staatsangehörigkeit ist nicht wichtig!

* Als datenschutzfreundliche Empfehlung haben wir eine Mieterselbstauskunft erstellt.

* Für die erste Kontaktaufnahme sind weder Schufa noch Adresse wichtig. Name und eine Kontaktmöglichkeit sollte ausreichen.

* Kein Ungefragtes Abbilden Ihrer neuen (oder alten) Mieter in Mietzeitschriften. Auch eine namentliche Erwähnung sollte **OHNE** Einverständnis nicht erfolgen.

* Fragen Sie keinen Scan des Personalausweises an.

* Geben Sie **keine Daten über ehemalige/derzeitige Vermieter an künftige Mieter weiter**. Dazu zählt auch der Name, auch nicht wenn der neue Mieter einen Internetanschluss einrichten möchte und "angeblich" dafür den Namen benötigt. Die TAE-Dosen Nummer reicht vollkommen.

* Überlegen Sie, ob eine Schufa wirklich aussagekräftig ist oder ob Ihnen die Kaution oder ein geschwärzter Kontoauszug genügt.

* Vermeiden Sie datenhungrige Email Adressen, wenn Sie mit Ihren Mietern kommunizieren. Empfehlungen für datenschutzbewusste Emails, haben wir bereits [hier](https://anoxinon.media/empfohlenedienste/) zusammengefasst.

* Vermeiden Sie die Kommunikation über Whatsapp mit Ihren Mietern, am besten nutzen Sie ein separates (gesichertes) Smartphone für Ihre Mieter oder altmodisch ein Dumbphone (ein nicht-Smartphone), um private von geschäftlichen Daten zu trennen und auch die IT-Sicherheit der Mieterdaten zu erhöhen.

* Verzichten Sie auf Videoüberwachung vor und in den Mietshäusern.

* Wenn Sie die Mietunterlagen ohne große Telemetrieübertragungen speichern wollen, empfehlen wir Ihnen Linux. In unseren vorherigen Artikeln haben wir bereits [die Gründe für Linux](https://anoxinon.media/blog/linuxfuereinsteiger1/) sowie eine [Installationsanleitung](https://anoxinon.media/blog/linuxfuereinsteiger2/) veröffentlicht.

* Öffnen Sie ein **separates Konto** für die Mieteinnahmen und verknüpfen Sie dies nicht mit ihrem privaten Konto und verbinden Sie diese Einnahmen mit keiner App oder Fintechs, die Daten auslesen können.

* **Mobile Endgeräte müssen abgesichert sein**, gerade auch wenn Sie "Embedded Devices" oder (z. B. auch eigens-programmierte) Apps nutzen, um z. B. direkt auf einem Tablet unterschreiben zu können oder die Heizungsstände einzuspeichern. Sie haben hierfür keinen gesonderten Endvertrag mit Google oder Apple (je nach Gerät) und müssen dafür sorgen, dass die Daten Ihrer Mieter das Gerät nicht verlassen.

* Kümmern Sie sich um **Auftragsverarbeitungsverträge** mit Ihren Dienstleistern (Handwerker, Dachdecker, Sanitärinstallateuren etc.) und stellen Sie hier auch sicher, dass Sie nicht unbefugt Kontaktinformationen Ihrer Mieter weitergeben und das sich die Dienstleister auch an den Datenschutz halten. Fragen Sie auch explizit nach der Nutzung von Whatsapp & Co.!

* Verzichten Sie auf smarte Feuermelder. **Kein Feuermelder der Welt benötigt ein Mikrofon**. Sie schädigen die Privatsphäre und Ihren Geldbeutel als Vermieter. Die Bedenken über den Einsatz von smarten Rauchmeldern war bereits [2015 ein Artikel im Spiegel erschienen](https://www.spiegel.de/netzwelt/gadgets/rauchmelder-star-datenschutz-streit-ueber-funk-rauchmelder-a-1064719.html).

* Kopieren Sie die Wohnungsgeberbestätigung lediglich und legen diese (nicht-digital erfasst) in Ihren Unterlagen ab. Diese Unterlagen können auch Daten von Personen enthalten, die nicht Teil des Mietvertrages sind.

Damit Vermieter sich hinter keinen faulen Ausreden verstecken können, haben wir einen Interessentenbogen
für Vermieter sowie eine "Freiwillige Selbstauskunft" für Mieter vorbereitet.

{{< link-button href="/files/vermietung/Interessentenbogen.pdf" label="Download Interessenbogen" >}}
{{< link-button href="/files/vermietung/Mieter_Selbstauskunft.pdf" label="Download Selbstauskunft" >}}

---

### VIII. Schlussworte<a name="Schlussworte"></a>

Die Wohnung ist grundrechtlich abgesicherter Rückzugsort des Individuums und seiner Privatsphäre. Die fast schon alltägliche Praxis von Vermietern, vor und während des Mietverhältnisses vom Vertragspartner absurde Datenmengen einzufordern, passt hiermit so gar nicht zusammen. Sie lässt vielmehr den Datenschutz als bloßes Lippenbekenntnis zurück. Soll ein Umdenken im Mietsektor stattfinden, müssen Datenschutzbeauftragte und staatliche Stellen im Allgemeinen Informationsarbeit mit eigenen (Formular-)Empfehlungen betreiben. Fehlreaktionen wie etwa das Anonymisieren von Klingelschildern - so geschehen in [Wien 2018](https://www.zeit.de/gesellschaft/zeitgeschehen/2018-10/oesterreich-wien-dsgvo-namensschilder-beschwerde-hausverwaltung) - tragen hier nicht mehr als eine skurrile Komik bei.
