+++
title = "Fediverse - Was ist das?"
date = "2019-01-30T12:29:00+01:00"
tags = ["Anfänger", "Fortgeschritten"]
categories = ["Freie Software", "Dezentrale Netzwerke"]
banner = "/img/thumbnail/fediverse_was_ist_das.png"
description = "Fedi- Bitte was?! - In diesem Beitrag möchten wir den Begriff Fediverse erklären, was dahinter steckt und die Vorteile und die Eigenheiten bzw. Risiken anschneiden."
introblock = "Dieser Beitrag ist für Anfänger geeignet"
+++

### I. Allgemeines

Bei sozialen Netzwerken denken die meisten wohl an Facebook und Twitter.
Aber ist das wirklich alles? Es gab ja auch mal Myspace, was schon vor 10 Jahren von Facebook abgehängt wurde. SchülerVZ gibt es nicht mehr, StudiVZ verliert ebenfalls massiv an Bedeutung und viele der anderen kleineren Plattformen schließen ihre Tore.
Die neuen Social Networks, die im Trend liegen - wie bspw. Instagram, Snapchat oder TikTok - warten mit neuen Konzepten auf. Mehr Fokus auf Inhalte, sowie hohe Nutzerbindung durch speziell entworfene Mechanismen, welche die Nutzer vom Dienst abhängig machen sollen.
Alle haben eines gemeinsam: Gewinnmaximierung des dahinter stehenden Unternehmens.

Das Fediverse (zusammengesetzt aus Fedi von Federation und verse von Universe) möchte einen Gegenpol zu diesen Entwicklungen stellen. Es ist nicht ein einzelner Betreiber, ein Unternehmen oder ein Dienst, sondern eine Föderation aus verschiedenster Software welche untereinander kommunizieren kann. Dazu gehören bspw. Mastodon, Pleroma oder Friendica. Das Fediverse ist dezentral aufgebaut. Dies bedeutet, dass jeder die Möglichkeit besitzt einen eigenen Server zu betreiben und an der Föderation teilzunehmen. Das Fediverse wird in Instanzen gegliedert. Der Begriff "Instanz" klingt ansprechender als Server und verdeutlicht wiederum, dass es nicht den einen ultimativen Server gibt. Alle Instanzen sind gleichberechtigt und agieren ohne zentrale, übergeordnete Anlaufstelle.


### II. Vorteile

Jede Instanz hat ihre eigenen Regeln. Meistens werden diese in einer Art Netiquette definiert – ein respektvoller Umgang miteinander unter Wahrung der geltenden Regeln.
Auch gibt es Instanzen, die sich eher einem gewissen politischen Spektrum zuordnen lassen oder sich für den amerikanisch geprägten Begriff „Free Speech“ einsetzen (in etwa: Man darf sich frei ausdrücken, solange kein anderer körperlich zu Schaden kommt).
Durch das Registrieren auf einer Instanz erhält man Zugang zum kompletten Fediverse. Man erhält einen Nutzernamen der aus zwei Teilen besteht. Part Eins ist der gewählte Name an sich und Part Zwei die Domain der gewählten Instanz. (Bsp: [@Anoxinon@social.anoxinon.de](https://social.anoxinon.de/@Anoxinon))  
Es ist nicht notwendig sich für jedes Netzwerk separat anzumelden. Als Teilnehmer des Fediverse kann man grundsätzlich jedem anderen Nutzer folgen und sich austauschen.

Der lokale Gemeinschaftsfaktor ist nicht zu unterschätzen. Jede Instanz hat eine eigene Diskussionskultur. Zum Beispiel gibt es Instanzen, die sich eher an technisch versierte Nutzer und Entwickler richtet, aber auch Instanzen für Einsteiger, für Fans bestimmter Popkulturen, Instanzen mit regionalem Bezug und noch viel mehr.

Es ist eine kleine Community, der man so viel oder so wenig Beachtung schenkt, wie man möchte. Schließlich können die Nutzer, denen man folgt, fast uneingeschränkt aus dem gesamten Fediverse ausgesucht werden. Die Eigenständigkeit der Instanz bleibt erhalten, aber es entsteht zugleich eine übergreifende Austauschmöglichkeit mit anderen Gruppen. Ob Nerds, Aktivisten, Musikfans usw., alle Nutzer können sich untereinander austauschen, wenn sie es möchten. Ein freundlicher und respektvoller Umgang miteinander zeichnet das Fediverse erfahrungsgemäß aus!

Sollte man als Nutzer wider Erwarten nicht mehr mit der Administration einverstanden sein, gibt es jederzeit die Möglichkeit auf andere Instanzen zu wechseln. Je nach Software ist ein Export/Import von Daten möglich.

Ein solch dezentales Netzwerk bietet natürlich sehr viele weitere Vorteile.
Die Software, die zum Betrieb einer Instanz genutzt wird, ist meist FOSS: quelloffen, kostenlos; definiert unter einer freien Lizenz wie z.B. der GPL.
Das bedeutet, dass jeder den Quellcode ansehen, also quasi einen Blick unter die Motorhaube werfen kann. Desweiteren sind Anpassungen für jede Instanz einzeln möglich. Mag man gewisse Funktionen nicht, wirft man sie eben heraus. Werden neue Funktionen programmiert, so stehen diese wiederum allen anderen Nutzern zur Verfügung.
Der größte daraus resultierende Vorteil ist die Offenheit des gesamten Netzwerkes.

>Wir haben bereits einen Beitrag veröffentlicht, der sich mit freier Software beschäftigt: [Ist freie Software seriös?](https://anoxinon.media/blog/istfreiesoftwareserioes/)


### III. Eigenheiten bzw. Risiken:

In der Regel werden Server im Fediverse von einzelnen Privatpersonen, kleineren Organisationen oder auch Vereinen betrieben. Gerade, wenn vor allem größere Instanzen von Einzelpersonen betrieben werden, gibt es einige Stolpersteine. Der Administrator hört auf und schließt den Dienst, oder ist plötzlich nicht mehr verfügbar. Für die einzelnen Nutzer beginnt dann die Suche nach einem neuen Zuhause, auf einer anderen Instanz. Dies ist zwar ärgerlich und mit Aufwand verbunden, aber aufgrund der Ausweichmöglichkeiten kein Beinbruch. Glücklicherweise kommt das nicht allzu oft vor.
Schwerwiegender ist es, wenn sich der/die Betreiber nicht mehr um die Instanz(en) kümmern. Von Sicherheitslücken, die den Server angreifbar machen, über fehlende Aktualisierungen für neue Funktionen oder gar den kompletten Verlust der Föderationsmöglichkeit, alles kann passieren.

Das Betreiben eines Servers sowie der angebotenen Dienste erfordern Zeit, Wissen und auch ein wenig Geld. Die Nachteile betreffen die großen, kommerziellen Anbieter aber genauso. Diese haben aber meist weitaus mehr Personal und Geldmittel zur Verfügung. Im Unterschied zu Twitter oder Facebook bricht im Fediverse aber nicht das komplette Netzwerk zusammen, wenn einzelne Server Probleme haben oder gar ausfallen. Vertrauen ist ebenso erforderlich, denn nicht nur die eigene Instanz, sondern auch alle verbundenen Instanzen müssen die eigene Privatsphäre respektieren. Bspw. könnten Privatnachrichten (DM) von den Administratoren der eigenen oder der Instanz des Empfängers über Umwege ausgelesen werden. Grundsätzlich basiert aber jegliche Nutzung eines Onlindienstes auf Vertrauen. Allgemein sollten sensible Informationen niemals über unverschlüsselte Wege versendet werden, unabhängig vom Diensteanbieter.

Für Neueinsteiger ist außerdem eine Umgewöhnung notwendig. So funktionieren Nutzernamen anders, die Suchfunktion und der Weg wie man andere Nutzer findet ebenso. Teilweise ist das durch die Funktionsweise des Fediverse bedingt. Des weiteren geändert ist die Usability, im Vergleich zu gewohnten Oberflächen. Hierbei gibt es jedoch Abhilfe. Ein gutes Beispiel ist [Halcyon](https://halcyon.anoxinon.de/login), dass eine Twitter-ähnliche Oberfläche bereitstellt und mit einem Mastodon/Pleroma Account benutzt werden kann.

Da die Software und das Fediverse aktiv weiterentwickelt werden, bleiben die zukünftigen Änderungen abzuwarten. Es steht jedoch jedem Nutzer frei, den Entwicklern Ihrer Software Vorschläge zu machen oder selber mitzuwirken.


### IV. Woraus besteht denn nun das Fediverse genau?

Nachfolgend nur eine kleine Auswahl verschiedenster Dienste. In Klammern stehen die nicht freien, bekannten Dienste, welche sich am ehesten mit den Fediverse-Netzwerken vergleichen lassen. Eine sehr gute Übersicht bietet die Seite [Fediverse Network](https://fediverse.network).


* [Mastodon](https://joinmastodon.org/) (Twitter): Eine Software für MicroBlogs, in welchem man sich direkt austauschen kann. Dabei können sogenannte Toots (Kurznachrichten) bis zu einer Länge von 500 Zeichen pro Eintrag verfasst werden. Toot bedeutet "etwas in die Welt tröten", also veröffentlichen.


* [Peertube](https://joinpeertube.org/) (YouTube): Plattform zum Bereitstellen von Videos - hochladen, ansehen, Kanäle abonnieren, kommentieren usw. All dies bietet Peertube ohne integriertes Tracking & Werbung - und das dezentral.


* [Friendica](https://friendi.ca/) (Facebook): Soziales Netzwerk für den Austausch und auch für längere Posts geeignet. Beinhaltet auch eine Kalenderfunktion.


* [Pixelfed](https://pixelfed.org/) (Instagram): Bilder austauschen, kommentieren, teilen usw.  


* [Funkwhale](https://funkwhale.audio/) (Spotify): Freier Austausch von Musik und Erstellung von Playlisten. Dabei werden alle gängigen Audio Formate unterstützt.


### V. Schlussworte

Wichtig wäre es noch zu erwähnen, dass viele der großen Instanzen die Registrierung für neue Nutzer öfters komplett aussetzen. Grund hierfür ist weniger die Serverkapazität, sondern die erwünschte Verteilung. Die Fediverse Besucher sollen sich auf möglichst viele Instanzen verteilen und nicht auf wenige große bzw. bekannte. Dies fördert die Vielfalt, Ausfallsicherheit und Dezentralisierung.

Nicht verschweigen möchten wir auch, dass die Anzahl der Nutzer im Vergleich zu den großen, kommerziellen Diensten noch deutlich geringer ist.

**Dies zu ändern liegt in unser aller Händen. Je mehr Menschen sich für freie Alternativen entscheiden, desto größer und belebter wird das Fediverse!**


P.S.: Dankeschön für euer Feedback und Input auf Mastodon!
